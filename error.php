<?php

class error
{


/*----------------------------------
	エラー時の文字列を入れる配列の宣言
------------------------------------*/
	private $error_array;


/*--------------------------------------------
	コンストラクタ
----------------------------------------------*/
	public function __construct($error_array)
	{


	/*-------------------------
		エラーがあった時
	---------------------------*/
		if($error_array != 0)
		{
			$this->error_array = $error_array;
		}


	/*-------------------------
		エラーがなかった時
	---------------------------*/
		else
		{
	        $this->error_array = array(		
									'lastname' => "",
	                                'firstname' => "",
	                                'ln_rb' => "",
	                                'fn_rb' => "",
	                                'nickname' => "",
	                                'sei' => "",
	                                'mail' => "",
	                                'pass' => "",
	                                'pass_conf' => "",
	                                'postcode' => "",
	                                'prefecture' => "",
	                                'address' => "",
	                                'building_address' => "",
	                                'phonenumber' => "",
	                                'user_wish_mail_magazine' => ""
			                    );
    	}
	}


/*-------------------------
	エラーメッセージを返す
---------------------------*/
	public function get_error_message()
	{
		$error_message = array();
		foreach ($this->error_array as $key => $value)
		{
			$error_message += array($key => $this->or_null($value));
		}

		return $error_message;
	}


/*-------------------------------
	nullだった時、空の文字列を返す
---------------------------------*/
	private function or_null($str)
	{
		if(!isset($str))
			return "";
		else
			return $str;
	}
}

?>