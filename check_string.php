<?php

class check_string
{


/*------------------
    フォームの配列
--------------------*/
	private $post_array;


/*----------------------
    エラーフラグ
------------------------*/
	private $error_flag = false;


/*---フォームの数値ー＞文字列に使う配列を宣言---------------------------------------------*/

    private $prefecture = array(
                "北海道","青森県","秋田県","岩手県","山形県","宮城県","福島県",
                "茨城県","栃木県","群馬県","埼玉県","神奈川県","千葉県","東京都",
                "山梨県","長野県","新潟県","富山県","石川県","福井県","岐阜県","静岡県",
                "愛知県","三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県",
                "鳥取県","島根県","岡山県","広島県","山口県","徳島県","香川県","愛媛県",
                "高知県","福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県"
            );

/*-----------------------------------------------------------------------------------*/


/*--------------------
    コンストラクタ
----------------------*/
	public function __construct($post_array)
	{
    

    /*---配列の初期化----------------------*/
		$this->post_array = array();
		$this->post_array_keys = array();
    /*------------------------------------*/


    /*---------------
        引数を代入
    -----------------*/
		$this->post_array = $post_array;

	}


/*---------------------------
    エラーメッセージを返す
-----------------------------*/
	public function get_error_message()
	{
		$error_array = array(); //配列の初期化

    /*---配列を繰り返す---*/
		foreach ($this->post_array as $key => $value)
		{
			switch ($key)
			{
				case 'lastname':
					$error_array['lastname'] = $this->check_name($value);
					break;

				case 'firstname':

					$error_array['firstname'] = $this->check_name($value);
					break;

				case 'ln_rb':

					$error_array['ln_rb'] = $this->check_ruby($value);
					break;

				case 'fn_rb':

					$error_array['fn_rb'] = $this->check_ruby($value);
					break;

				case 'nickname':

					$error_array['nickname'] = $this->check_nickname($value);
					break;

				case 'sei':

					$error_array['sei'] = $this->check_sei($value);
					break;

				case 'mail':

					$error_array['mail'] = $this->check_mail($value);
					break;

				case 'pass':

					$error_array['pass'] = $this->check_pass($value);
					break;

				case 'pass_conf':

					$error_array['pass_conf'] = $this->check_pass_conf($this->post_array['pass'], $value);
					break;

				case 'postcode':

					$error_array['postcode'] = $this->check_postcode($value);
					break;

				case 'prefecture':

					$error_array['prefecture'] = $this->check_prefecture($value);
					break;

				case 'address':

					$error_array['address'] = $this->check_address($value);
					break;

				case 'building_address':

					$error_array['building_address'] = $this->check_building_address($value);
					break;

				case 'phonenumber':

					$error_array['phonenumber'] = $this->check_phonenumber($value);
					break;
				case 'message':

					$error_array['message'] = $this->check_message($value);
					break;


				case 'user_wish_mail_magazine':

					$error_array['user_wish_mail_magazine'] = $this->check_wish_mail_magazine($value);
					break;
			}
		}

		return $error_array;
	}


/*----------------------------------------
    エラーフラグを返す
------------------------------------------*/
	public function is_set_error_flag()
    {
		return $this->error_flag;
	}


/*------------------------------------
    value値が数字の値ー＞文字列
--------------------------------------*/
    public function get_confirm_form()
    {
        $confirm_form = $this->post_array;

        $confirm_form['sei'] = $this->convert_sex();

        $confirm_form['pass'] = $this->convert_pass();

        $confirm_form['pass_conf'] = $this->convert_pass();

        $confirm_form['prefecture'] = $this->convert_prefecture();
        
        $confirm_form['user_wish_mail_magazine'] = $this->convert_wish_mail();

        return $confirm_form;
    }


/*------------------------------------
    性別のvalue値を文字列に変換
--------------------------------------*/
    private function convert_sex()
    {
        if($this->post_array['sei'] == 1)
        {
            return "男性";
        }
        else if($this->post_array['sei'] == 2)
        {
            return "女性";
        }
        else
        {
            return "エラー";
        }
    }


/*------------------------------------
    性別のvalue値を文字列に変換
--------------------------------------*/
    private function convert_pass()
    {
        $pass_convert = "";

        for($i = 0; $i < strlen($this->post_array['pass']); $i++)
        {
            $pass_convert = $pass_convert."●";
        }
        return $pass_convert;
    }


/*-----------------------------------------
    都道府県の数値ー＞文字列
-------------------------------------------*/
    private function convert_prefecture()
    {
        return $this->prefecture[ $this->post_array['prefecture'] - 1 ];

    }


/*-----------------------------------------
    メール受信の数値ー＞文字列
-------------------------------------------*/
    private function convert_wish_mail()
    {
        if($this->post_array['user_wish_mail_magazine'] == "1")
        {
            return "受信する";
        }
        else if($this->post_array['user_wish_mail_magazine'] == "2")
        {
            return "受信しない";
        }
    }


/*--------------------------------------
    nameフォームのチェック
----------------------------------------*/
	private function check_name($str)
    {
    	if(preg_match("/^[ぁ-んァ-ヶ一-龠々a-zA-Z]+$/", $str))
        {
            return null;
	    }
        else
        {
	    	$this->error_flag = true;
	    	return "名前を入力してください";
	    }
    }


/*----------------------------------------
    rubyフォームのチェック
------------------------------------------*/
    private function check_ruby($str)
    {
    	if(preg_match("/^[ぁ-ん]+$/u", $str))
        {
    		return null;
        }
        else
        {
    		$this->error_flag = true;
    		return "平仮名でふりがなを入力してください";
    	}
    }


/*-------------------------------------------
    nicknameフォームのチェック
---------------------------------------------*/
    private function check_nickname($str)
    {
    	if(preg_match("/^.+$/", $str))
        {
	    	return null;
    	}
        else
        {
    		$this->error_flag = true;
    		return "ニックネームを入力してください";
    	}
    }


/*-------------------------------------
    seiフォームのチェック
---------------------------------------*/
    private function check_sei($str)
    {
    	if($str == "1" || $str == "2")
        {
			return null;
		}
        else
        {
			$this->error_flag = true;
			return "姓を選んでください";
		}
    }


/*-------------------------------------
    mailフォームのチェック
---------------------------------------*/
	private function check_mail($str)
	{
        if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])+@([a-zA-Z0-9])+([a-zA-Z0-9\._-]+)+$/", $str))
        {
        	return null;
        }
        else
        {
        	$this->error_flag = true;
            return "正しいメールアドレスを入力してください";
        }
	}


/*-------------------------------------
    passフォームのチェック
---------------------------------------*/
	private function check_pass($str)
    {
    	if(!empty($str))
        {
    		if(strlen($str) < 6 )
            {
    			$this->error_flag = true;
    			return "パスワードが短すぎます";
    		}
            else
            {
                if(preg_match("/^[0-9a-zA-Z]+$/", $str))
                {
                    return null;
                }
    			else
                {
                    $this->error_flag = true;
                    return "使用できない文字列が含まれています";
                }
    		}
    	}
        else
        {
    		$this->error_flag = true;
    		return "パスワードを入力してください";
    	}
    }


/*-------------------------------------
    pass_confフォームのチェック
---------------------------------------*/
    private function check_pass_conf($str1,$str2)
    {
        if(strcmp($str1, $str2) == 0)
        {
			return null;
		}
        else
        {
			$this->error_flag = true;
			return "パスワードが一致していません";
		}
    }


/*-------------------------------------------
    postcodeフォームのチェック
---------------------------------------------*/
    private function check_postcode($str)
    {
        if(preg_match("/^[0-9]{3}-[0-9]{4}$/", $str) || preg_match("/^[0-9]{7}$/", $str))
        {
			return ;
		}
        else
        {
			$this->error_flag = true;
			return "郵便番号を入力してください";
    	}
    }


/*---------------------------------------------
    prefectureフォームのチェック
-----------------------------------------------*/
	private function check_prefecture($str)
    {
        if(preg_match("/^[0-9]{1,2}$/", $str))
        {
			return null;
		}
        else
        {
			$this->error_flag = true;
			return "都道府県を選んでください";
		}
    }


/*------------------------------------------
    addressフォームのチェック
--------------------------------------------*/
    private function check_address($str)
    {
    	if(!empty($str))
        {
    		return null;
    	}
        else
        {
    		$this->error_flag = true;
    		return "住所を入力してください";
    	}
    }


/*---------------------------------------------------
    building_addressフォームのチェック
-----------------------------------------------------*/
    private function check_building_address($str)
    {
    	if(!empty($str))
        {
    		return null;
    	}
        else
        {
    		return null;
    	}
    }


/*----------------------------------------------
    phonenumberのチェック
-------------------------------------------------*/
    private function check_phonenumber($str)
    {
    	if(!empty($str))
        {
    		if(preg_match("/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/", $str) || preg_match("/^[0-9]{10,12}$/", $str))
            {
    			return null;
    		}
            else
            {
    			$this->error_flag = true;
    			return "電話番号の形式で入力してください";
    		}
    	}
        else
        {
    		return null;
    	}
    }


/*---------------------------------------------
    messageフォームのチェック
-----------------------------------------------*/
    private function check_message($str)
    {
    	if(!empty($str))
        {
    		return null;
    	}
        else
        {
    		return null;
    	}
    }


/*-----------------------------------------------------
    user_wish_mail_magazineフォームのチェック
-------------------------------------------------------*/
    private function check_wish_mail_magazine($str)
    {
    	if(isset($str))
        {
    		if($str == "1")
            {
                return null;
    		}
            else if($str == "2")
            {
                return null;
    		}
            else
            {
                $this->error_flag = true;
                return "メールを受信するか選んでください";
            }
    	}
        else
        {
    		$this->error_flag = true;
    		return "メールを受信するか選んでください";
    	}
    }
}

?>