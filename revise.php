<?php

class revise
{


/*--------------------
	フォーム値の入れ物
----------------------*/
	private $form_value;


/*----------------
	県の配列を宣言
-----------------*/
	private $prefecture = array(
				"北海道","青森県","秋田県","岩手県","山形県","宮城県","福島県",
				"茨城県","栃木県","群馬県","埼玉県","神奈川県","千葉県","東京都",
				"山梨県","長野県","新潟県","富山県","石川県","福井県","岐阜県","静岡県",
				"愛知県","三重県","滋賀県","京都府","大阪府","兵庫県","奈良県","和歌山県",
				"鳥取県","島根県","岡山県","広島県","山口県","徳島県","香川県","愛媛県",
				"高知県","福岡県","佐賀県","長崎県","熊本県","大分県","宮崎県","鹿児島県","沖縄県"
			);


/*--------------------------------------------
	コンストラクタ
----------------------------------------------*/
	public function __construct($form_value)
	{


	/*---------------------
		エラー、修正の時
	-----------------------*/
		if($form_value != 0)
		{
			$this->form_value = $form_value;
		}


	/*---------------------
		初めてサイトに訪れる時
	-----------------------*/
		else
		{
	        $this->form_value = array(
		        					'lastname' 					=> "",
		                            'firstname' 				=> "",
		                            'ln_rb' 					=> "",
		                            'fn_rb' 					=> "",
		                            'nickname' 					=> "",
		                            'sei' 						=> "",
		                            'mail' 						=> "",
		                            'pass' 						=> "",
		                            'pass_conf' 				=> "",
		                            'postcode' 					=> "",
		                            'prefecture' 				=> "",
		                            'address' 					=> "",
		                            'building_address' 					=> "",
		                            'phonenumber' 				=> "",
		                            'message' 					=> "",
		                            'user_wish_mail_magazine' 	=> ""
		                        );
	    }

	}


/*---------------------
	元のフォーム値を送る
-----------------------*/
	public function get_form_value()
	{
		$form_value_set = array();

		foreach ($this->form_value as $key => $value)
		{
			$form_value_set += array($key => $this->or_null($value));
		}

		return $form_value_set;
	}


/*------------------------
	nullの時、空の文字列に変換
--------------------------*/
	private function or_null($str)
	{
		if(!isset($str))
		{
			return "";
		}
		else
		{
			return htmlspecialchars($str);
		}
	}


/*---------------------
	性別値を返す
-----------------------*/
	public function is_checked_sex()
	{
		if(!empty($this->form_value['sei']))
		{
			if($this->form_value['sei'] == "1")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}


/*---------------------
	都道府県フォームを作成
-----------------------*/
	public function make_prefecture()
	{
		$prefecture_form = array();
		$i = 1;

		if(empty($this->form_value['prefecture']))
		{
			array_push($prefecture_form,'<option value="0">（未選択）</option>');

			foreach ($this->prefecture as $key => $value)
			{
				array_push($prefecture_form,'<option value="'.$i++.'">'.$value.'</option>');
			}
		}
		else
		{
			foreach ($this->prefecture as $key => $value)
			{
				if($key + 1 == $this->form_value['prefecture'])
				{
					array_push($prefecture_form,'<option value="'.$i++.'" selected>'.$value.'</option>');
				}
				else
				{
					array_push($prefecture_form,'<option value="'.$i++.'">'.$value.'</option>');
				}
			}
		}

		return $prefecture_form;
	}


/*-----------------------------
	メールを受信するかの値を返す
-------------------------------*/
	public function is_checked_wish_mail()
	{
		if(!empty($this->form_value['user_wish_mail_magazine']))
		{
			if($this->form_value['user_wish_mail_magazine'] == "1")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
}