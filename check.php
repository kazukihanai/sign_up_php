<?php

/*---------------------------
    セッションスタート
----------------------------*/
    session_start();


/*------------------------------
    文字列チェッククラスの読み込み
--------------------------------*/
    include 'check_string.php';


/*------------------------------------
    初めての訪問の場合はindexページへ
--------------------------------------*/
    if(empty($_POST))
    {
        header('Location: index.php');
        exit;
    }


/*---------------------------
    配列の宣言
----------------------------*/
    $_SESSION['form_value'] = array();


/*---------------------------
    クリックジャッキング対策
----------------------------*/
    header('X-FRAME-OPTIONS:DENY');


/*---------------------------
    CSRF対策
----------------------------*/
    if($_SESSION['hidden_pass'] != $_POST['token'])
    {
        header('Location: index.php');
        exit;
    }


/*--------------------------------------------------------------
    xss対策,仮名変換,NULLバイト攻撃対策
----------------------------------------------------------------*/
    foreach ($_POST as $key => $value)
    {
        $_SESSION['form_value'] = $_SESSION['form_value'] + array($key => htmlspecialchars($value));

        $_SESSION['form_value'][$key] = mb_convert_kana($_SESSION['form_value'][$key],"asH");
        
        $_SESSION['form_value'][$key] = str_replace("\0", "", $_SESSION['form_value'][$key]);
    }
/*--------------------------------------------------------------*/


/*----------------------------
    文字列チェッククラスの生成
------------------------------*/
    $check_string = new check_string($_SESSION['form_value']);


/*----------------------------
    エラーメッセージの取得
------------------------------*/
    $_SESSION['error_message'] = $check_string->get_error_message();


/*---------------------------------
    エラーがあったら前のページに戻る
-----------------------------------*/
    if($check_string->is_set_error_flag())
    {
        header('Location: index.php');
        exit;
    }


/*----------------------------
    フォームの数値ー＞文字列
------------------------------*/
    $confirm_form = $check_string->get_confirm_form();

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>会員登録</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/script.js"></script>
</head>
    <body>
      <div id="all">
      	<h1>入力確認</h1>
      	<p>入力した内容でよろしいでしょうか？</p>

    <!--    名前   -->
      	<dl>
          <dt>お名前</dt>
          <dd>
        		姓： <?php print $confirm_form['lastname'] ?><br>
        		名： <?php print $confirm_form['firstname'] ?>
          </dd>

    <!--    ふりがな   -->
        </dl>
        <dl>
          <dt>ふりがな</dt>
          <dd>
        		せい： <?php print $confirm_form['ln_rb'] ?><br>
        		めい： <?php print $confirm_form['fn_rb'] ?>
          </dd>
        </dl>

    <!--   ニックネーム    -->
        <dl>
          <dt>ニックネーム</dt>
          <dd><?php print $confirm_form['nickname'] ?></dd>
        </dl>

    <!--    性別   -->
        <dl>
          <dt>性別</dt>
          <dd><?php print $confirm_form['sei'] ?></dd>
        </dl>

    <!--   メールアドレス    -->
        <dl>
          <dt>メールアドレス</dt>
          <dd><?php print $confirm_form['mail'] ?></dd>
        </dl>

    <!--   パスワード    -->
        <dl>
          <dt>パスワード</dt>
          <dd><?php print $confirm_form['pass'] ?></dd>
        </dl>

    <!--   パスワード確認    -->
        <dl>
          <dt>パスワード確認</dt>
          <dd><?php print $confirm_form['pass'] ?></dd>
        </dl>

    <!--   郵便番号    -->
        <dl>
          <dt>郵便番号</dt>
          <dd><?php print $confirm_form['postcode'] ?></dd>
        </dl>

    <!--    都道府県   -->
        <dl>
          <dt>都道府県</dt>
          <dd><?php print $confirm_form['prefecture'] ?></dd>
        </dl>

    <!--   住所    -->
        <dl>
          <dt>住所</dt>
          <dd><?php print $confirm_form['address'] ?></dd>
        </dl>

    <!-- 　  住所（建物名）    -->
        <dl>
          <dt>住所（建物名）</dt>
          <dd><?php print $confirm_form['building_address'] ?></dd>
        </dl>

    <!--   電話番号    -->
        <dl>
          <dt>電話番号</dt>
          <dd><?php print $confirm_form['phonenumber'] ?></dd>
        </dl>

    <!--   メッセージ    -->
        <dl>
          <dt>メッセージ</dt>
          <dd><?php print $confirm_form['message'] ?></dd>
        </dl>

    <!--   運営からのお知らせ    -->
      	<dl>
          <dt>運営からのお知らせ</dt>
          <dd><?php print $confirm_form['user_wish_mail_magazine'] ?></dd>
      	</dl>

        <a href="index.php">修正する</a>
      	<a href="complete.php" >確認</a>
      </div>
    </body>
</html>