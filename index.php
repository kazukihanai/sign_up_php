<?php

/*---------------------------
    セッションスタート
----------------------------*/
    session_start();


/*---------------------------
    外部ファイル読み込み
----------------------------*/
    include 'error.php';
    include 'revise.php';


/*---------------------------
    クリックジャッキング対策
----------------------------*/
    header('X-FRAME-OPTIONS:DENY');


/*---------------------------
    CSRF対策
----------------------------*/
    $hidden_pass = rand(1000000,9999999);
    $_SESSION["hidden_pass"] = $hidden_pass;


/*---------------------------------
    エラークラス、
    フォーム初期値クラス の宣言
-----------------------------------*/
    if(isset($_SESSION['error_message']))
    {
        $error = new error($_SESSION['error_message']);
    }
    else
    {
        $error = new error(0);
    }

    if(isset($_SESSION['form_value']))
    {
        $revise = new revise($_SESSION['form_value']);
    }
    else
    {
        $revise = new revise(0);
    }

/*---------------------------
    セッションの値をクリア
-----------------------------*/
    unset($_SESSION['error_message']);
    unset($_SESSION['form_value']);

/*---------------------------
    エラー値のセット
-----------------------------*/
    $error_array = $error->get_error_message();

/*---------------------------
    フォーム値のセット
-----------------------------*/
    $form_value = $revise->get_form_value();

    $is_checked_sex = $revise->is_checked_sex();

    $prefecture_form = $revise->make_prefecture();

    $is_checked_wish_mail = $revise->is_checked_wish_mail();

?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>会員登録</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="https://zipaddr.github.io/zipaddrx.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
    <div id="all">
        <h1>会員登録</h1>
        <h2>会員登録フォーム</h2>
        <form action="check.php" method="post">
            
        <!--    名前（姓名）   -->
            <dl>
                <dt>
                    <label for="name">お名前</label> <em>必須</em>
                </dt>
                <dd>
                    姓<input name="lastname" type="text" value="<?php print $form_value['lastname'] ?>">
                    名<input name="firstname" type="text" value="<?php print $form_value['firstname'] ?>">

                    <p id="error"> <?php print $error_array['lastname']?> </p>
                    <p id="error"> <?php print $error_array['firstname']?> </p>
                </dd>
            </dl>

        <!--    名前のふりがな   -->
            <dl>
                <dt>
                    <label for="ruby">ふりがな</label> <em>必須</em>
                </dt>
                <dd>
                    せい<input name="ln_rb" placeholder="" type="text" value="<?php print $form_value['ln_rb'] ?>">
                    めい<input name="fn_rb" placeholder="" type="text" value="<?php print $form_value['fn_rb'] ?>">

                    <p id="error"> <?php print $error_array['ln_rb']?> </p>
                    <p id="error"> <?php print $error_array['fn_rb']?> </p>
                </dd>
            </dl>

        <!--    ニックネーム   -->
            <dl>
                <dt>
                    <label for="nickname">ニックネーム</label> <em>任意</em>
                </dt>
                <dd>
                    <input name="nickname" type="text" value="<?php print $form_value['nickname'] ?>">

                    <p id="error"> <?php print $error_array['nickname']?> </p>
                </dd>
            </dl>

        <!--   性別    -->
            <dl>
                <dt>
                    性別<em>必須</em>
                </dt>
                <dd>
                    <input <?php if($is_checked_sex) print "checked" ?> name="sei" type="radio" value="1">
                    <label for="sex">男性</label>
                    <input <?php if(!$is_checked_sex) print "checked" ?> name="sei" type="radio" value="2">
                    <label for="sex">女性</label>

                    <p id="error"> <?php print $error_array['sei']?> </p>
                </dd>
            </dl>

        <!--   メールアドレス    -->
            <dl>
                <dt>
                    <label for="email">メールアドレス</label> <em>必須</em>
                </dt>
                <dd>
                    <input name="mail" type="text" value="<?php print $form_value['mail'] ?>">

                    <p id="error"> <?php print $error_array['mail']?> </p>
                </dd>
            </dl>

        <!--    パスワード   -->
            <dl>
                <dt>
                    <label for="password">パスワード</label> <em>必須</em>
                </dt>
                <dd>
                    <input name="pass" type="password" value="<?php print $form_value['pass'] ?>">
                    半角英数字6文字以上を設定してください。

                    <p id="error"> <?php print $error_array['pass']?> </p>
                </dd>
            </dl>

        <!--    パスワード（確認）   -->
            <dl>
                <dt>
                    <label for="password">パスワード(確認)</label> <em>必須</em>
                </dt>
                <dd>
                    <input name="pass_conf" type="password" value="<?php print $form_value['pass_conf'] ?>">

                    <p id="error"> <?php print $error_array['pass_conf']?> </p>
                </dd>
            </dl>

        <!--   郵便番号    -->
            <dl>
                <dt>
                    <label for="pcode1">郵便番号</label> <em>必須</em>
                </dt>
                <dd>
                    <input type="text" id="zip" name="postcode" value="<?php print $form_value['postcode'] ?>">

                    <p id="error"> <?php print $error_array['postcode']?> </p>
                </dd>
            </dl>

        <!--   都道府県    -->
            <dl>
                <dt>
                    <label for="address">都道府県</label> <em>必須</em>
                </dt>
                <dd>
                    <select id="pref" name="prefecture">
                        <?php
                        /*-----------------------
                            都道府県フォームの出力
                        -------------------------*/
                            foreach ($prefecture_form as $value)
                            {
                                print $value;
                                print PHP_EOL; //改行
                            }
                        ?>
                    </select><br>

                    <p id="error"> <?php print $error_array['prefecture']?> </p>
                </dd>
            </dl>

        <!--    住所   -->
            <dl>
                <dt>
                    <label for="address">住所</label> <em>必須</em>
                </dt>
                <dd>
                    <input type="text" id="addr" name="address" value="<?php print $form_value['address'] ?>">

                    <p id="error"> <?php print $error_array['address']?> </p>
                </dd>
            </dl>

        <!--    住所（建物名）   -->
            <dl>
                <dt>
                    <label for="address">住所（建物名）</label> <em>任意</em>
                </dt>
                <dd>
                    <input type="text" name="building_address" value="<?php print $form_value['building_address'] ?>">

                    <p id="error"> <?php print $error_array['building_address']?> </p>
                </dd>
            </dl>

        <!--    電話番号   -->
            <dl>
                <dt>
                    <label for="tel">電話番号(半角)</label> <em>任意</em>
                </dt>
                <dd>
                    <input type="text" name="phonenumber" value="<?php print $form_value['phonenumber'] ?>">

                    <p id="error"> <?php print $error_array['phonenumber']?> </p>
                </dd>
            </dl>

        <!--    メッセージ   -->
            <dl>
                <dt>
                    <label for="tel">メッセージ</label> <em>任意</em>
                </dt>
                <dd>
                    <textarea name="message"><?php print $form_value['message'] ?></textarea>
                </dd>
            </dl>

        <!--    運営からのお知らせ   -->
            <dl>
                <dt>
                    運営からのお知らせ <em>必須</em>
                </dt>
                <dd>
                    <label class="radio-inline">
                        <input <?php if($is_checked_wish_mail) print "checked" ?> id="user_wish_mail_magazine_1" name="user_wish_mail_magazine" type="radio" value="1"> 受信する
                    </label>
                    <label class="radio-inline">
                        <input <?php if(!$is_checked_wish_mail) print "checked" ?> id="user_wish_mail_magazine_0" name="user_wish_mail_magazine" type="radio" value="2"> 受信しない
                    </label>

                    <p id="error"> <?php print $error_array['user_wish_mail_magazine']?> </p>
                </dd>
            </dl>

       <!--      トークン     -->
            <input type="hidden" name="token" value="<?php print $hidden_pass ?>">
        
        
            <button type="submit">確認画面へ</button>
        <!--     </form>     -!>
        </form>
    </div>
</body>
</html>