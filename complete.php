<?php


/*---------------------------
    セッションスタート
-----------------------------*/
	session_start();


/*---------------------------
    クリックジャッキング対策
-----------------------------*/
    header('X-FRAME-OPTIONS:DENY');


/*------------------------------
    メール送信結果の初期値を設定
--------------------------------*/	
	$success_mail_master = "管理者にメールを送信できませんでした。";
	$success_mail_custormer = "お客様にメールを送信できませんでした。";


/*----------------------------
    お客様用メールの要素を作成
------------------------------*/
	if(isset($_SESSION['form_value']))
	{
		$to_customer = 		$_SESSION['form_value']['mail'];
		$to_master        = 'kazuki.hanai@fourier.jp';
		$subject_customer = 'ご登録ありがとうございました';
		$subject_master =   '会員登録の通知';
		$message_customer = 'ありがとうございました';


	/*-------------------------------------------管理者用メール本文の作成-----------------------------------------*/

		$lastname =    "姓：　"              				.$_SESSION['form_value']['lastname'];
		$firstname =   "名：　"              				.$_SESSION['form_value']['firstname'];
		$ln_rb =       "せい：　"            				.$_SESSION['form_value']['ln_rb'];
		$fn_rb =       "めい：　"            				.$_SESSION['form_value']['fn_rb'];
		$nickname =    "ニックネーム：　"     				.$_SESSION['form_value']['nickname'];
		$sei =         "性別：　"            				.$_SESSION['form_value']['sei'];
		$mail =        "メールアドレス：　"   					.$_SESSION['form_value']['mail'];
		$pass =        "パスワード：　"       				.$_SESSION['form_value']['pass'];
		$pass_conf =   "パスワード（確認）：　"				.$_SESSION['form_value']['pass_conf'];
		$postcode =    "郵便番号：　"         				.$_SESSION['form_value']['postcode'];
		$prefecture =  "県：　"              				.$_SESSION['form_value']['prefecture'];
		$address =     "住所：　"            				.$_SESSION['form_value']['address'];
		$building_address =    "住所（建物名）：　"   			.$_SESSION['form_value']['building_address'];
		$phonenumber = "電話番号：　"        					.$_SESSION['form_value']['phonenumber'];
		$message =     "メッセージ：　"      					.$_SESSION['form_value']['message'];
		$user_wish_mail_magazine = "運営からのお知らせ："		.$_SESSION['form_value']['user_wish_mail_magazine'];

		$message_master = 	$lastname."\n".
						    $firstname."\n".
						    $ln_rb."\n".
							$fn_rb."\n".
							$nickname."\n".
							$sei."\n".
							$mail."\n".
							$pass."\n".
							$pass_conf."\n".
							$postcode."\n".
							$prefecture."\n".
							$address."\n".
							$building_address."\n".
							$phonenumber."\n".
							$message."\n".
							$user_wish_mail_magazine."\n";
	/*--------------------------------------------------------------------------------------------------------*/


	/*-----------------------------------
	    管理者用メールのヘッダーを作成
	-------------------------------------*/
		$headers = 'From: test@test.test' . "\r\n";


	/*--------------------------お客様メールを送信-----------------------------*/

		if(mb_send_mail($to_customer, $subject_customer, $message_customer, $headers))
		{
			$success_mail_custormer = "お客様にメールを送信しました。";

		}
	/*-----------------------------------------------------------------------*/


	/*----------------------------管理者メールを送信----------------------*/
		
		if(mb_send_mail($to_master, $subject_master, $message_master, $headers))
		{
			$success_mail_master = "管理者にメールを送信しました。";

		}
	/*------------------------------------------------------------------*/


	/*------------------------
	    セッションをクリア
	--------------------------*/
		unset($_SESSION['form_value']);

	}

?>

<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>会員登録</title>
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
	<div id="all">
		<?php print $success_mail_custormer ?><br>
		<?php print $success_mail_master ?><br>
		<a href="index.php">トップページへ戻る</a>
	</div>
</body>
</html>

